<?php

require_once("autoload.php");

class User extends Connection
{
    private $Name;
    private $Phone_num;
    private $email;
    private $conn;

public function __construct()
{
    $this->conn = new Connection ;
    $this->conn = $this->conn->connection ;
}

public function insertUser(string $strName, string $strPhone, string $strEmail)
{
    $this->name = $strName;
    $this->phone_num= $strPhone;
    $this->email = $strEmail;
    
    $sql = "INSERT INTO user(name,,email) VALUES(?,?,?) ";
    $insert = $this->conn->prepare($sql);
    $arrData = array($this->name, $this->phone_num ,$this->email,);
    $resInsert = insert->execute($arrData);
    $lastId = $this->conn->lastInsertId();
    return $lastId;
}
public function getUser()
{
    $sql = "SELECT * FROM `user`";
    $get = $this->conn->query($sql);
    $resGet = $get->fetchall(PDO::FECTH_ASSOC);
    return $resGet;
}
public function updateUser(int $id,string $strName, string $strPhone, string $strEmail)
{
    $this->name = $strName;
    $this->phone_num= $strPhone;
    $this->email = $strEmail;
    
    $sql = "UPDATE user set name=? phone_num=?, email=? WHERE id=$id";
    $insert = $this->conn->prepare($sql);
    $arrData = array($this->name, $this->phone_num ,$this->email,);
    $resUpdate = insert->execute($arrData);
    return $resUpdate;    
}
public function getUpdate(int $id)
{
     $sql = "SELECT * FROM user WHERE id=?";
     $query = $this->conn->prepare($sql);
     $arrwhere = array ($id);
     $query->execute($arrwhere);
     $request = $query->fetch(PDO::FECTH_ASSOC);
     return $request;
}
public function deleteUser(int $id)
{
     $sql = "DELETE * FROM user WHERE id=?";
     $delete = $this->conn->prepare($sql);
     $arrwhere = array (id);
     $del = $delete->execute($arrwhere);
     return $del;
}
}